#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_H
#define HEADER_H

using namespace std;
using namespace arma;

void qr_gs_decomp(mat &A, mat &R);
void backsub(mat& R, vec& c, vec& x);
void qr_gs_solve(mat &Q, mat &R, vec &b);
void qr_gs_inverse(mat &R, mat &Q, mat &B);

void givens_qr(mat &A);
void givens_qr_QTvec(mat &QR, vec &v);
void givens_qr_solve(mat &A, vec &v, vec &x);


#endif
