#include "decomp.h"


void qr_gs_decomp(mat &A, mat &R){
  int m = A.n_cols;
  for(int i=0; i<m; i++){
    R(i,i) = norm(A.col(i));
    A.col(i) = A.col(i)/R(i,i);
    for(int j=i+1; j<m; j++){
      R(i,j) = dot(A.col(i),A.col(j));
      A.col(j) -= R(i,j)*A.col(i);
    }
  }
}

void backsub(mat& R, vec& c, vec& x){ // solves Rx = c
  int n = x.size();
  for(int i=n-1; i>=0; i--){
    x(i) = c(i); //Q.col(i) =Qt.row(i)
    for(int j=i+1; j<n; j++){
      x(i) -= R(i,j)*x(j);
    }
    x(i) /= R(i,i);
  }
}

void qr_gs_solve(mat &Q, mat &R, vec &b){
    vec c = Q.t()*b;
    backsub(R,c,b);
}



void qr_gs_inverse(mat &R, mat &Q, mat &B){ // A(Bi) = QR(Bi) = Ii
  int n = R.n_cols;
  mat I = eye(n,n);
  vec Bi(n);
  for(int i=0; i<n; i++){
    vec c = Q.t()*I.col(i);
    backsub(R, c, Bi);
    B.col(i) = Bi;
  }
}

// Calculates matrix which have R in upper triangle and the
// theta angles (used for calculating G = QT) in the ramaining entrances.
void givens_qr(mat &A){
  int m = A.n_cols;
  int n = A.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = atan(A(p,q)/A(q,q));
      for(int k=q; k<m; k++){
        double rq = A(q,k), rp = A(p,k);
        A(q,k) = cos(theta)*rq + sin(theta)*rp;
        A(p,k) = -sin(theta)*rq + cos(theta)*rp;
      }
      A(p,q) = theta;
    }
  }
}

// Calculates right part of Rx = Q^Tb
void givens_qr_QTvec(mat &QR, vec &v){
  int m = QR.n_cols;
  int n = QR.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = QR(p,q);
      double vq = v(q), vp = v(p);
      v(q) = cos(theta)*vq + sin(theta)*vp;
      v(p) = -sin(theta)*vq + cos(theta)*vp;
    }
  }
}

// solve Rx = Q^Tb by backsubstitution.
void givens_qr_solve(mat &A, vec &v, vec &x){
  givens_qr(A);
  givens_qr_QTvec(A,v);
  backsub(A,v,x);
}
