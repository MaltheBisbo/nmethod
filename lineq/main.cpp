#include "decomp.h"


int main(){
  arma_rng::set_seed_random(); // seed random number generator


  //*** Part A1 ***//
  cout << "PART A1" << endl;
  cout << "I check that qr_gs_decomp work on non-square matrices with N>M" << endl;
  int N0=6;
  int M0=4;
  mat A0(N0,M0,fill::randu), R0(M0,M0,fill::zeros);
  vec b0(N0,fill::randu);

  cout << "A =\n"<< A0 << endl;
  mat Q0 = A0;
  qr_gs_decomp(Q0,R0);
  cout << "A = Q*R=\n"<< Q0*R0 << endl;
  cout << "Qt*Q =\n" << Q0.t()*Q0 << endl;
  cout << "R =\n" << R0 << endl;

  cout << "This shows that the decomposition works." << endl;

  //*** Part A2 ***//
  cout << "PART A2" << endl;
  cout << "Solve system of equations Ax=b with A beeing square" << endl;

  int N=5;
  int M=5;
  mat A(N,M,fill::randu), R(M,M,fill::zeros);
  mat B(N,M,fill::zeros);
  vec b(N,fill::randu), x(M), y(M);

  // solve Ax = b or similarely QRx = b
  // this is solved by Rx = (Qt)b
  mat Q = A;
  qr_gs_decomp(Q,R);
  x = b;
  qr_gs_solve(Q,R,x);
  cout << "x =\n" << x << endl;
  cout << "b =\n" << b << endl;
  cout << "Ax =\n" << A*x << endl;
  cout << "Which equals b and thus shows that the solution is correct" << endl;

  //*** Part B ***//
  qr_gs_inverse(R, Q, B);
  cout << "\n\nPART B" << endl;
  cout << "AB =\n" << (Q*R)*B << endl;
  cout << "which is the identity, and B must therefore be the inverse of A" << endl;

  //*** Part C ***//
  givens_qr_solve(A,b,y);
  cout << "\n\nPART C" << endl;
  cout << "y =\n" << y << endl;
  cout << "This is the same solution as found in part A" << endl;

  return 0;
}
