#include "interp.h"

int main(){
  cout << "Tests of the linear, quadratic and cubic splines are shown in plot.pdf" << endl;
  double z = 2.9;
  int N = 20;
  double xmax = 7;
  vector<double> x(N), y(N);
  vector<double> x_l(N), y_l(N), integ_l(N); // l for linear spline
  //*** Make cosine function to spline ***//
  ofstream file;
  file.open("data.dat", ios::trunc);
  file << "x\tdata" << endl;
  for(int i=0; i<N; i++){
    x[i]=i*xmax/(N-1);
    y[i]=cos(i*xmax/(N-1));
    file << x[i] << "\t" << y[i] << endl;
  }

  //*** Make linear spline + integral based on linear spline ***//
  interp l;
  l.setlinterp(x,y);
  file << "\n\n" << "x\tlspline\tlspline_integ" << endl;
  for(int i=0; i<N; i++){
    double zi = i*xmax/(N-1);
    x_l[i] = zi;
    y_l[i] = l.lspline_eval(zi);
    integ_l[i] = l.lspline_integ(zi);
    file << x_l[i] << "\t" << y_l[i] << "\t" << integ_l[i] << endl;
  }
  //*** Make quadratic spline + derivative + integral ***//
  int Nspl = 100;
  vector<double> x_q(Nspl), y_q(Nspl), integ_q(Nspl), diff_q(Nspl);
  interp q;
  q.setqinterp(x,y);
  file << "\n\n" << "x\tqspline\tqspline_integ\tqspline_deriv" << endl;
  for(int i=0; i<Nspl-1; i++){
    double zi = (i+0.5)*xmax/(Nspl-1);
    x_q[i] = zi;
    y_q[i] = q.qspline_eval(zi);
    integ_q[i] = q.qspline_integ(zi);
    diff_q[i] = q.qspline_deriv(zi);
    file << x_q[i] << "\t" << y_q[i] << "\t" << integ_q[i] << "\t" << diff_q[i] << endl;
  }

  //*** Make cubic spline + derivative + integral ***//
  vector<double> x_c(Nspl), y_c(Nspl), integ_c(Nspl), diff_c(Nspl);
  interp c;
  c.setcinterp(x,y);
  file << "\n\n" << "x\tcspline\tcspline_integ\tcspline_deriv" << endl;
  for(int i=0; i<Nspl-1; i++){
    double zi = (i+0.5)*xmax/(Nspl-1);
    x_c[i] = zi;
    y_c[i] = c.cspline_eval(zi);
    integ_c[i] = c.cspline_integ(zi);
    diff_c[i] = c.cspline_deriv(zi);
    file << x_c[i] << "\t" << y_c[i] << "\t" << integ_c[i] << "\t" << diff_c[i] << endl;
  }

  return 0;
}
