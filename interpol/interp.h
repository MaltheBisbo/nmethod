#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <stdlib.h>
#ifndef HEADER_H
#define HEADER_H

using namespace std;

class interp{
private:
  int n;
  vector<double> dx;
  vector<double> x;
  vector<double> y;
  vector<double> b;
  vector<double> c;
  vector<double> d;
public:
  interp();
  ~interp();
  void setlinterp(vector<double> x_vec, vector<double> y_vec);
  void setqinterp(vector<double> x_vec, vector<double> y_vec);
  void setcinterp(vector<double> x_vec, vector<double> y_vec);
  void setx(vector<double> x_vec);
  void sety(vector<double> y_vec);
  void setn();
  int binSearch(double z);
  double lspline_eval(double z);
  double lspline_integ(double z);
  double qspline_eval(double z);
  double qspline_integ(double z);
  double qspline_deriv(double z);
  double cspline_eval(double z);
  double cspline_integ(double z);
  double cspline_deriv(double z);
};


#endif
