#include "interp.h"

interp::interp(){}
interp::~interp(){}

void interp::setlinterp(vector<double> x_vec, vector<double> y_vec){
  setx(x_vec); sety(y_vec); setn();
}

void interp::setx(vector<double> x_vec){
  x = x_vec;
}
void interp::sety(vector<double> y_vec){
  y = y_vec;
}
void interp::setn(){
  n = x.size();
}

int interp::binSearch(double z){
  int R=n-1, L=0, m;
  while(R-L>1){
    m=floor((R+L)/2.0);
    if(z>x[m]) L=m;
    else R=m;
  }
  return L;
}

double interp::lspline_eval(double z){
  int m = binSearch(z);
  return y[m] + (z-x[m])/(x[m+1]-x[m])*(y[m+1]-y[m]);
}

double interp::lspline_integ(double z){
  int m = binSearch(z);
  double integz = 0;
  for(int i=0; i<m; i++){
    double ai = (y[i+1]-y[i])/(x[i+1]-x[i]);
    integz += ai*(pow(x[i+1],2)-pow(x[i],2))/2 + (y[i]-ai*x[i])*(x[i+1]-x[i]);
  }
  double am = (y[m+1]-y[m])/(x[m+1]-x[m]);
  integz += am*(pow(z,2)-pow(x[m],2))/2 + (y[m]-am*x[m])*(z-x[m]);
  return integz;
}

// q-spline

void interp::setqinterp(vector<double> x_vec, vector<double> y_vec){
  setx(x_vec); sety(y_vec); setn();
  vector<double> p(n-1);
  for(int i=0; i<n-1; i++){
    dx.push_back(x[i+1]-x[i]);
    p[i] = (y[i+1]-y[i])/dx[i];
  }
  c.push_back(0);
  for(int i=0; i<n-2; i++){
    c.push_back( 1/dx[i+1]*(p[i+1]-p[i]-c[i]*dx[i]) );
  }
  c[n-2]/=2;
  for(int i=n-3; i>=0; i--){
    c[i] = 1/dx[i]*(p[i+1]-p[i]-c[i+1]*dx[i+1]);
  }
  for(int i=0; i<n-1; i++){
    b.push_back(p[i] - c[i]*dx[i]);
  }
}

double interp::qspline_eval(double z){
  int m = binSearch(z);
  return y[m] + b[m]*(z-x[m]) + c[m]*pow(z-x[m],2);
}

double interp::qspline_integ(double z){
  int m = binSearch(z);
  double integ = 0;
  for(int i=0; i<m; i++){
    integ += y[i]*dx[i] + b[i]/2*pow(dx[i],2) + c[i]/3*pow(dx[i],3);
  }
  integ += y[m]*(z-x[m]) + b[m]/2*pow(z-x[m],2) + c[m]/3*pow(z-x[m],3);
  return integ;
}

double interp::qspline_deriv(double z){
  int m = binSearch(z);
  return b[m] + 2*c[m]*(z-x[m]);
}

void interp::setcinterp(vector<double> x_vec, vector<double> y_vec){
  setx(x_vec); sety(y_vec); setn();
  vector<double> p(n-1), D(n), Q(n-1), B(n), Dt(n), Bt(n);
  for(int i=0; i<n-1; i++){
    dx.push_back(x[i+1]-x[i]);
    p[i] = (y[i+1]-y[i])/dx[i];
  }
  D[0] = 2;
  for(int i=1; i<n-1; i++){
    D[i] = 2*dx[i-1]/dx[i]+2;
  }
  D[n-1] = 2;
  Q[0] = 1;
  for(int i=1; i<n-1; i++){
    Q[i] = dx[i-1]/dx[i];
  }
  B[0] = 3*p[0];
  for(int i=1; i<n-1; i++){
    B[i] =3*(p[i-1]+p[i]*dx[i-1]/dx[i]);
  }
  B[n-1] = 3*p[n-2];
  Dt[0] = D[0];
  for(int i=1; i<n; i++){
    Dt[i] = D[i] - Q[i-1]/Dt[i-1];
  }
  Bt[0] = B[0];
  for(int i=1; i<n; i++){
    Bt[i] = B[i] - Bt[i-1]/Dt[i-1];
  }
  b.resize(n);
  b[n-1] = Bt[n-1]/Dt[n-1];
  for(int i=n-2; i>=0; i--){
    b[i] = (Bt[i] - Q[i]*b[i+1])/Dt[i];
    //cout << "b[" << i << "] = " << b[i] << endl;
  }
  for(int i=0; i<n-1; i++){
    c.push_back( (-2*b[i] - b[i+1] + 3*p[i])/dx[i] );
    d.push_back( (b[i] + b[i+1] - 2*p[i])/pow(dx[i],2) );
  }
}

double interp::cspline_eval(double z){
  int m = binSearch(z);
  return y[m] + b[m]*(z-x[m]) + c[m]*pow(z-x[m],2) + d[m]*pow(z-x[m],3);
}

double interp::cspline_integ(double z){
  int m = binSearch(z);
  double integ = 0;
  for(int i=0; i<m; i++){
    integ += y[i]*dx[i] + b[i]/2*pow(dx[i],2) + c[i]/3*pow(dx[i],3) + d[i]/4*pow(dx[i],4);
  }
  integ += y[m]*(z-x[m]) + b[m]/2*pow(z-x[m],2) + c[m]/3*pow(z-x[m],3) + d[m]/4*pow(z-x[m],4);
  return integ;
}

double interp::cspline_deriv(double z){
  int m = binSearch(z);
  return b[m] + 2*c[m]*(z-x[m]) + 3*d[m]*pow(z-x[m],2);
}
