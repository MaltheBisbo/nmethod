#include "integ.h"

double integ_adapt14(function<double(double)> f, double a, double b, double f2,
                     double f3, double acc, double eps, int Ncalls){
  assert(Ncalls<1e6);
  double f1 = f(a+(b-a)*1.0/6);
  double f4 = f(a+(b-a)*5.0/6);
  double Q = (2*f1+f2+f3+2*f4)*(b-a)/6;
  double q = (f1+f2+f3+f4)*(b-a)/4;
  double tol = acc+eps*fabs(Q);
  double err = fabs(Q-q);
  if(err<tol){
    return Q;
  }else{
    double Q1 = integ_adapt14(f,a,(b+a)/2,f1,f2,acc/sqrt(2),eps,Ncalls+1);
    double Q2 = integ_adapt14(f,(b+a)/2,b,f3,f4,acc/sqrt(2),eps,Ncalls+1);
    return Q1+Q2;
  }
}

double integ_adapt(function<double(double)> f, double a, double b, double acc, double eps){
  int Ncalls = 0;
  double f2, f3;
  if(isinf(a) && isinf(b)){
    function<double(double)> f_new = [f](double x){return (f((1-x)/x)+f(-(1-x)/x))/(x*x);};
    a=0;b=1;
    f2 = f_new(a+(b-a)*2.0/6);
    f3 = f_new(a+(b-a)*4.0/6);
    return integ_adapt14(f_new,a,b,f2,f3,acc,eps,Ncalls);
  } else if(!isinf(a) && isinf(b)){
    function<double(double)> f_new = [f,a](double x){return f(a+(1-x)/x)/(x*x);};
    a=0; b=1;
    f2 = f_new(a+(b-a)*2.0/6);
    f3 = f_new(a+(b-a)*4.0/6);
    return integ_adapt14(f_new,a,b,f2,f3,acc,eps,Ncalls);
  } else if(isinf(a) && !isinf(b)){
    function<double(double)> f_new = [f,b](double x){return f(b-(1-x)/x)/(x*x);};
    a=0; b=1;
    f2 = f_new(a+(b-a)*2.0/6);
    f3 = f_new(a+(b-a)*4.0/6);
    return integ_adapt14(f_new,a,b,f2,f3,acc,eps,Ncalls);
  } else{
    f2 = f(a+(b-a)*2.0/6);
    f3 = f(a+(b-a)*4.0/6);
    return integ_adapt14(f,a,b,f2,f3,acc,eps,Ncalls);
  }
}
