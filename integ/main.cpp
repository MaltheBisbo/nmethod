#include "integ.h"
#include "../ode/ode.h"

double func1(double x){N++; return sqrt(x);}
double func2(double x){N++; return 1/sqrt(x);}
double func3(double x){N++; return log(x)/sqrt(x);}
double func4(double x){N++; return 4*sqrt(1-pow(1-x,2));}
double func5(double x){N++; return exp(-x*x);}
double func6(double x){N++; return x*exp(-x*x);}
double func7(double x){N++; return (x+x*x)*exp(-x*x);}

vec diffeq(double x,vec& y){
  N++;
  vec dydt(1);
  dydt(0) = 4*sqrt(1-pow(1-x,2));
  return dydt;
}

int N; // global variable;

int main(){
  cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
  cout.precision(20);

  //********** PART A ***********//
  cout << "//******* PART A *******//" << endl;

  N=0;
  double acc = 1e-10, eps = 1e-10;
  double a = 0, b = 1;
  double Q = integ_adapt(func1, a, b, acc, eps);
  cout << "\nsqrt(x) from 0 to 1:" << endl;
  cout << "Q = " << Q << endl;
  cout << "Should be = 2/3" << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  N=0;
  acc = 1e-10; eps = 1e-10;
  a = 0; b = 1;
  Q = integ_adapt(func2, a, b, acc, eps);
  cout << "\n1/sqrt(x) from 0 to 1:" << endl;
  cout << "Q = " << Q << endl;
  cout << "Should be = 2" << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  N=0;
  acc = 1e-8; eps = 1e-8;
  a = 0; b = 1;
  Q = integ_adapt(func3, a, b, acc, eps);
  cout << "\nlog(x)/sqrt(x) from 0 to 1:" << endl;
  cout << "Q = " << Q << endl;
  cout << "Should be = -4" << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  N=0;
  acc = 1e-15; eps = 1e-15;
  a = 0; b = 1;
  Q = integ_adapt(func4, a, b, acc, eps);
  cout << "\n1-(1-x)^2 from 0 to 1:" << endl;
  cout << "\tQ = " << Q << endl;
  cout << "Should be = 3.141592653589793238" << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  //********** PART B ***********//
  cout << "//******* PART B *******//" << endl;

  N=0;
  acc = 1e-7; eps = 1e-7;
  a = -INFINITY; b = INFINITY;
  Q = integ_adapt(&func5, a, b, acc, eps);
  cout << "\nexp(-x^2) from -inf to inf:" << endl;
  cout << "\tQ = " << Q << endl;
  cout << "Should be = " << sqrt(M_PI) << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  N=0;
  acc = 1e-7; eps = 1e-7;
  a = 0; b = INFINITY;
  Q = integ_adapt(&func6, a, b, acc, eps);
  cout << "\nx*exp(-x^2) from 0 to inf:" << endl;
  cout << "\tQ = " << Q << endl;
  cout << "Should be = " << 0.5 << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  N=0;
  acc = 1e-7; eps = 1e-7;
  a = -INFINITY; b = 0;
  Q = integ_adapt(&func7, a, b, acc, eps);
  cout << "\n(x+x^2)*exp(-x^2) from -inf to 0:" << endl;
  cout << "\tQ = " << Q << endl;
  cout << "Should be = " << (sqrt(M_PI)-2)/4 << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;

  //********** PART C ***********//
  cout << "\n\n//******* PART C *******//" << endl;

  N=0;
  double x_a = 0, x_b = 1;
  vec y0 = {func4(0)};
  vec xs(1); xs(0) = x_a;
  mat ys(1,1); ys.col(0) = y0;
  double h = 1e-4;
  acc = 1e-6; eps = 1e-6;
  driver(&diffeq, xs, x_b, h, ys, acc, eps);

  cout << "Integrating: " << endl;
  cout << "\n1-(1-x)^2 from 0 to 1:" << endl;
  cout << "as an ODE gives: " << endl;
  cout << "\tQ = " << ys.col(ys.n_cols-1) << endl;
  cout << "Should be = 3.141592653589793238" << endl;
  cout << "Number of integrand evaluations: N = " << N << endl;
  cout << "Which is a factor 100 less evaluations compared to" << endl;
  cout << "the adaptive integrator." << endl;


  return 0;
}
