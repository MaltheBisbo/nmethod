#include <armadillo>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <functional>
#include <assert.h>


#ifndef HEADER_H
#define HEADER_H

using namespace std;
using namespace arma;

extern int N;

double integ_adapt(function<double(double)> f, double a, double b, double acc, double eps);
double integ_adapt14(function<double(double)> f, double a, double b,
                     double f2, double f3, double acc, double eps, int Ncalls);

#endif
