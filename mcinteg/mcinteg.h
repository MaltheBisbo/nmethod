#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_ODE
#define HEADER_ODE

using namespace std;
using namespace arma;

void randx(int dim, vec &a, vec &b, vec &x);
void plainMC(double (*f)(vec &x), int dim, vec &a, vec &b, int N,
             double &result, double &error);


#endif
