#include "mcinteg.h"
//#include <time.h>

double func1(vec &u){
  double x=u(0), y=u(1), z=u(2);
  return y*y*z-x*x +3*x;
}

double func2(vec &x){
  return pow(M_PI,-3)/(1-cos(x(0))*cos(x(1))*cos(x(2)));
}

int main(){
  srand(time(NULL));

  //************ PART A *************//
  cout << "//************ PART A *************//\n" << endl;

  int N = 1e6;
  int dim = 3;
  vec a1 = {0,0,0};
  vec b1 = {1,1,1};
  vec x(dim);
  double result, error;
  plainMC(&func1, dim, a1, b1, N, result, error);
  cout << "analytic result = 1.33333" << endl;
  cout << "result = " << result << endl;
  cout << "error = " << error << endl;



  vec a2 = {0,0,0};
  vec b2 = {M_PI,M_PI,M_PI};
  plainMC(&func2, dim, a2, b2, N, result, error);
  cout << "\nanalytic result = 1.39320" << endl;
  cout << "result = " << result << endl;
  cout << "error = " << error << endl;

  //************ PART B *************//
  cout << "//************ PART B *************//\n" << endl;
  ofstream file;
  file.open("data.dat", ios::trunc);

  file << "N\terror" << endl;
  for(int i=0; i<50; i++){
    N = 100 + i*200;
    plainMC(&func1, dim, a1, b1, N, result, error);
    file << N << "\t" << error << "\t" << endl;
  }

  cout << "the error as function of N is shown in plot.pdf," << endl;
  cout << " where it is fitted by eye with 3.75/sqrt(N)." << endl;


  return 0;
}
