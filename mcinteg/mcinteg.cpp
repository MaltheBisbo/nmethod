#include "mcinteg.h"

void randx(int dim, vec &a, vec &b, vec &x){
  for(int i=0; i<dim; i++){
    x(i) = a(i) + (double)rand()/RAND_MAX*(b(i)-a(i));
  }
}

void plainMC(double (*f)(vec &x), int dim, vec &a, vec &b, int N, 
             double &result, double &error){
  vec x(dim);
  double V = 1, sum=0, sum2=0, fx;
  for(int i=0; i<dim; i++){
    V *= b(i)-a(i);
  }
  for(int i=0; i<N; i++){
    randx(dim, a, b, x);
    fx = f(x);
    sum += fx;
    sum2 += fx*fx;
  }
  double f_avg = sum/N;
  double f_var = sum2/N - f_avg*f_avg;
  result = f_avg*V;
  error = V*f_var/sqrt(N);
}
