#include "jacDecomb.h"

int main(int argc, char const *argv[]){
  //arma_rng::set_seed_random();
  int N = atoi(argv[1]);
  mat A(N,N,fill::randu);
  A = symmatu(A);
  int Neig=1;
  mat V(N,N);
  vec E(N);
  vec Nvec = jac_valBYval_decomb(A,V,E,Neig);

  if(N<6){
    cout << "***PART B***" << endl;
    cout << "The lowest eigenvalue of this 5x5 matrix was found in ";
    cout << Nvec(0) << " sweeps of the first row." << endl;
    cout << "The eigenvalue is:" << endl;
    cout << "E = " << E(0) << endl;
  }


  cout << "\nWhy does this method give the smallest eigenvalue first?" << endl;
  cout << " theta will lie in the interval [0,pi/2] such that 2csApq in App' and Aqq'" << endl;
  cout << " is always positive. This means that when zeroing the first row p=0, the" << endl;
  cout << " 2csApq term is always subtracted from the first diagonal element and added" << endl;
  cout << " to all others. As a result the eigenvalues arrise in ascending order.\n" << endl;
  cout << "The largest eigenvalue is found first by adding pi/2 to theta such that" << endl;
  cout << "cos(theta)*sin(theta) becomes negative and the sign of the 2csApq term" << endl;
  cout << "in App and Aqq is changed." << endl;

  return 0;
}
