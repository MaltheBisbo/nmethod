#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_H
#define HEADER_H

using namespace std;
using namespace arma;

int jac_cyclic_decomb(mat &A, mat &V, vec &E);
vec jac_valBYval_decomb(mat &A, mat &V, vec &E, int N);
vec jac_valBYval_decomb_largest(mat &A, mat &V, vec &E, int N);
int jac_classic_decomb(mat &A, mat &V, vec &E);
mat roundZero(mat A);
Col<int> max_index_rows(mat &A);
int max_search(vec v, int p);

#endif
