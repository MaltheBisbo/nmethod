#include "jacDecomb.h"

int main(int argc, char const *argv[]){
  //arma_rng::set_seed_random();
  int N = atoi(argv[1]);
  mat A(N,N,fill::randu);
  A = symmatu(A);
  mat Acopy = A;
  mat V(N,N);
  vec E(N);
  int Nsweeps = jac_cyclic_decomb(A,V,E);

  if(N<6){
    cout << "***PART A***" << endl;
    cout << "The matrix was diagonalized in " << Nsweeps << " sweeps." << endl;
    cout << "We check that D = VT*A*V is diagonal:" << endl;
    cout << "VT*A*V = \n" << roundZero(V.t()*Acopy*V) << endl;
    cout << "The eigenvalues are:" << endl;
    cout << "E = \n" << E << endl;
    cout << "The eigenvectors are:" << endl;
    cout << "V = \n" << V << endl;
  }

  return 0;
}
