#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_JACOBI
#define HEADER_JACOBI

using namespace std;
using namespace arma;


int jac_classic_decomb(mat &A, mat &V, vec &E);
Col<int> max_index_rows(mat &A);
int max_search(vec v, int p);
mat roundZero(mat A);

#endif
