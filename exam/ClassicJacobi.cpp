#include "ClassicJacobi.hh"

int jac_classic_decomb(mat &A, mat &V, vec &E){
  int n = A.n_rows, m = A.n_cols;
  int changed, sweeps = 0;
  V = eye(n,m);
  E = A.diag();
  Col<int> max_index = max_index_rows(A);
  do{
    changed = 0;
    for(int p=0; p<n-1; p++){
      int q = max_index(p);
      double Ep = E(p), Eq = E(q), Apq = A(p,q);
      double theta = atan2(2*Apq,Eq-Ep)/2;
      double s=sin(theta), c=cos(theta);

      double Ep1 = c*c*Ep + s*s*Eq - 2*s*c*Apq;
      double Eq1 = s*s*Ep + c*c*Eq + 2*s*c*Apq;
      if(Ep1 != Ep && Eq1 != Eq){
        sweeps++;
        changed = 1;

        // Apply Jacobi transformation to A
        E(p) = Ep1;
        E(q) = Eq1;
        A(p,q) = 0.0;
        for(int i=0; i<p; i++){
          double Ap = A(i,p), Aq = A(i,q);
          A(i,p) = c*Ap - s*Aq;
          A(i,q) = s*Ap + c*Aq;
        }
        for(int i=p+1; i<q; i++){
          double Ap = A(p,i), Aq = A(i,q);
          A(p,i) = c*Ap - s*Aq;
          A(i,q) = s*Ap + c*Aq;
        }
        for(int i=q+1; i<m; i++){
          double Ap = A(p,i), Aq = A(q,i);
          A(p,i) = c*Ap - s*Aq;
          A(q,i) = s*Ap + c*Aq;
        }

        // Update vector with index of largest element in (upper part of) each row.
        for(int i=0; i<p; i++){
          if( fabs(A(i,q)) > fabs(A(i,max_index(i))) ){ max_index(i) = q; }
          if( fabs(A(i,p)) > fabs(A(i,max_index(i))) ){ max_index(i) = p; }
        }
        for(int i=p+1; i<q+1; i++){
          if( fabs(A(p,i)) > fabs(A(p,max_index(p))) ){   max_index(p) = i; }
        }
        for(int i=q+1; i<n; i++){
          if( fabs(A(p,i)) > fabs(A(p,max_index(p))) ){   max_index(p) = i; }
          if( fabs(A(q,i)) > fabs(A(q,max_index(q))) ){ max_index(q) = i; }
        }

        // Update V
        for(int k=0; k<m; k++){
          double Vp = V(k,p), Vq = V(k,q);
          V(k,p) = c*Vp - s*Vq;
          V(k,q) = s*Vp + c*Vq;
        }
      }
    }
  }while (changed != 0);
  return sweeps;
}


Col<int> max_index_rows(mat &A){
  int n = A.n_rows;
  Col<int> max_index(n-1);
  for(int i=0; i<n-1; i++){
    max_index(i) = max_search((A.row(i)).t(),i);
  }
  return max_index;
}

int max_search(vec v, int p){
  int i_max = p+1;
  int n = v.n_elem;
  double maxval = 0.0;
  for(int i=p+1; i<n; i++){
    if(fabs(v(i))>maxval){
      maxval = fabs(v(i));
      i_max = i;
    }
  }
  return i_max;
}

mat roundZero(mat A){
  int n = A.n_rows, m = A.n_cols;
  mat C(n,m);
  for(int i=0; i<n; i++){
    for(int j=0; j<m; j++){
      if(fabs(A(i,j))<1e-7) {C(i,j) = 0.0;}
      else {C(i,j) = A(i,j);}
    }
  }
  return C;
}
