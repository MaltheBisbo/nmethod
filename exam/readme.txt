Exam problem:

I have chosen problem 8:
"Jacobi diagonalization with classic sweeps and indexing"

The Jacobi eigenvalue algorithm is used to diagonalize real symmetric matrices.

The algorithm iteratively applies certain orthorginal transformations J (Jacobi rotations)
to a matrix A, which zeroes pairs of off-diagonal elements (Apq and Aqp).

The algorithm relies on the fact that after the a Jacobi rotation, the sum of
squares of all the off-diagonal elements is reduced. The method therefore converges.

The amount of reduction in the sum of squares of the off-diagonal elements
depends on the absolute size of the zeroed elements. The larger these elements
are, the larger the reduction is.

This motivates the classical Jacobi method, which I have implemented.
In this method one sweeps through the rows of the upper triangle and eliminates
the largest absolute value in each row.

In the beginning of the algorithm one creates a vector holding the indexes of
the largest value in each row. This is done in O(n²) time. During the algorithm
this vector is updated after each transformation, which takes only O(n) time,
since only O(n) elements are changed by a transformation.

All the transformations amount to the matrix V = J_0*...*J_N, which holds the
eigenvectors and the diagonal matrix D = V^T A V resulting from applying the Jacobi
transformations on A until convergence holds the eigenvalues.
