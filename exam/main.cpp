#include "ClassicJacobi.hh"

int main(){
  arma_rng::set_seed_random();
  int N = 8;
  mat A(N,N,fill::randu);
  A = symmatu(A);
  mat Acopy = A;
  mat V(N,N); // Matrix for eigenvectors.
  vec E(N); // Vector for eigenvalues.
  int Ntrans = jac_classic_decomb(A,V,E);

  mat D = diagmat(E);

  cout << "The matrix was diagonalized in " << Ntrans << " Jacobi transformations.\n" << endl;

  cout << "Check that D = VT*A*V is diagonal:" << endl;
  cout << "VT*A*V = \n" << roundZero(V.t()*Acopy*V) << endl << endl;

  cout << "Check that V D V^T = A :" << endl;
  cout << "A - V D V^T =\n" << roundZero(Acopy-V*D*V.t()) << endl << endl;

  cout << "The eigenvalues are:" << endl;
  cout << "E = \n" << E << endl << endl;

  cout << "The eigenvectors are:" << endl;
  cout << "V = \n" << V << endl;


  // Number of sweeps as function of matrix size N

  ofstream file;
  file.open("data.dat",ios::trunc);

  Col<int> Nvec(10);
  for(int i=0; i<10; i++){
    Nvec(i) = 2+20*i;
  }
  file << "N\tNtrans" << endl;
  for(int i=0; i<10; i++){
    N = Nvec(i);
    mat A2(N,N,fill::randu);
    A2 = symmatu(A2);
    mat V2(N,N);
    vec E2(N);
    int Ntrans2 = jac_classic_decomb(A2,V2,E2);
    file << N << "\t" << Ntrans2 << endl;
  }

  cout << "\nThe number of Jacobi transformations as a function of\n matrix size N is shown in plow.png." << endl;
  cout << "It is manually fitted by 2.33*x^2, which fits well." << endl;

  return 0;
}
