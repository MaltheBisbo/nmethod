#include <armadillo>
#include <iostream>
#include <math.h>
#include<functional>

#ifndef HEADER_ODE
#define HEADER_ODE

using namespace std;
using namespace arma;

void rkstep12(vec (*f)(double,vec), double t, double h, vec &y, vec &yh, vec &err);
void rkstep2_twostep(vec (*f)(double,vec), double t, double h, vec &y, vec &yh, vec &err);
void driver(vec (*f)(double, vec&), vec& ts, double b, double& h,
            mat& ys, double acc, double eps);
void driver_twostep(vec (*f)(double, vec&), vec& ts, double b, double& h,
            mat& ys, double acc, double eps);

#endif
