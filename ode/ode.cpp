#include "ode.h"

void rkstep12(vec (*f)(double,vec&), double t, double h,
              vec &y, vec &yh, vec &dy){
  vec k0 = f(t,y);
  double t1 = t+h;
  vec y1 = y+h*k0;
  vec k1 = f(t1,y1);
  yh = y+h*0.5*(k0+k1);
  dy = h*0.5*(k1-k0); // yh - yh*
}

void rkstep2_twostep(vec (*f)(double,vec&), double t, double h,
              vec &y, vec &yh, vec &dy){
  int n = y.n_elem;
  vec y_full(n), y_twohalf(n);

  rkstep12(f, t, h, y, y_full, dy);
  rkstep12(f, t, h/2, y, y_twohalf, dy);
  rkstep12(f, t+h/2, h/2, y_twohalf, yh, dy);
  dy = (yh-y_full)/(2*2-1); // Runge's principle
}

void driver(vec (*f)(double, vec&), vec& ts, double b, double& h,
            mat& ys, double acc, double eps){
  int i=0;
  double a = ts(i);
  int n = ys.n_rows;
  vec y(n), dy(n);
  while(ts(i)<b){
    double t0 = ts(i);
    vec y0 = ys.col(i);
    if(t0+h>b) {h=b-t0;}
    rkstep12(f, t0, h, y0, y, dy);
    double err = norm(dy);
		double tol = (acc+norm(y)*eps)*sqrt(h/(b-a));
    if(err<tol){
			ts.resize(i+2);
      ts(i+1) = t0+h;
			ys.insert_cols(i+1,y);
      i++;
		}
		if(err>0) h = h*pow(tol/err,0.25)*0.95;
		else      h = 2*h;
  }
}

void driver_twostep(vec (*f)(double, vec&), vec& ts, double b, double& h,
            mat& ys, double acc, double eps){
  int i=0;
  double a = ts(i);
  int n = ys.n_rows;
  vec y(n), dy(n);
  while(ts(i)<b){
    double t0 = ts(i);
    vec y0 = ys.col(i);
    if(t0+h>b) {h=b-t0;}
    rkstep2_twostep(f, t0, h, y0, y, dy);
    double err = norm(dy);
		double tol = (acc+norm(y)*eps)*sqrt(h/(b-a));
    if(err<tol){
			ts.resize(i+2);
      ts(i+1) = t0+h;
			ys.insert_cols(i+1,y);
      i++;
		}
		if(err>0) h = h*pow(tol/err,0.25)*0.95;
		else      h = 2*h;
  }
}
