#include "ode.h"

int N;

vec diffeq(double t,vec& y){
  N++;
  vec dydt(2);
  dydt(0) = y(1);
  dydt(1) = -y(0);
  return dydt;
}


int main(){
  cout << "§§§------ PART A+B+C ------§§§\n\n" << endl;
  ofstream file;
  file.open("data.dat",ios::trunc);

  double t0 = 0, b = 2*3.14;
  vec y0 = {0,1};
  vec ts(1); ts(0) = t0; vec ts2 = ts;
  mat ys(2,1); ys.col(0) = y0; mat ys2 = ys;
  double h = 1e-4, acc = 1e-6, eps = 1e-6;

  N = 0;
  driver(&diffeq, ts, b, h, ys, acc, eps);
  cout << "The number of evaluations used by the 12-method was: " << N << endl;

  N = 0;
  driver_twostep(&diffeq, ts2, b, h, ys2, acc, eps);
  cout << "The number of evaluations used by the twostep-method was: " << N << endl;

  cout << "\nThe twostep-method is thus superior" << endl;

  file << "t\ty0\ty1" << endl;
  for(int i=0; i<abs(ts.n_elem); i++){
    file << ts(i) << "\t" << ys(0,i) << "\t" << ys(1,i) << endl;
  }

  file << "\n\n" << endl;
  file << "t\ty0\ty1" << endl;
  for(int i=0; i<abs(ts2.n_elem); i++){
    file << ts2(i) << "\t" << ys2(0,i) << "\t" << ys2(1,i) << endl;
  }

  cout << "The solution to the ODE system is shown in plot.pdf." << endl;

  return 0;
}
