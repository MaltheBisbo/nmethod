#include "root.h"

int main(){
  vec x(2), fx(2), x0 = {3.0,3.3};
  double dx = 1e-4, epsilon = 1e-10;
  int N = 0;

  //********** PART A **********//
  cout << "PART A\n" << endl;

  //**** First set of equations ****//
  cout << "First set of equatons:\n" << endl;
  cout << "Solving the system of equations:" << endl;
  cout << "f0(x,y) = Axy - 1 = 0," << endl;
  cout << "f1(x,y) = exp(-x) + exp(-y) - 1 - 1/A = 0" << endl;
  cout << "with A = 10000" << endl;
  newton_root(&funcs1, x, x0, dx, epsilon, N);

  cout << "The number of function calls was: " << N << endl;
  cout << "The system has a root at:" << endl;
  cout << "x = \n" << x << endl;
  cout << "Where the function values are:" << endl;
  cout << "f(x) = \n" << funcs1(N,x) << endl;
  cout << "\nWe also have:" << endl;
  cout << "norm(fx) = " << norm(funcs1(N,x)) << endl;
  cout << "Which is below the demanded precision." << endl;

  //**** Gradient of Rosenbrock's Valley function ****//
  cout << "\nGradient of Rosenbrock's Valley function:\n" << endl;
  N=0;
  newton_root(&grad_Rosen, x, x0, dx, epsilon, N);

  cout << "The number of function calls was: " << N << endl;
  cout << "The system has a root at:" << endl;
  cout << "x = \n" << x << endl;
  cout << "Where the function values are:" << endl;
  cout << "f(x) = \n" << grad_Rosen(N,x) << endl;
  cout << "\nWe also have:" << endl;
  cout << "norm(fx) = " << norm(grad_Rosen(N,x)) << endl;
  cout << "Which is below the demanded precision." << endl;

  //**** Gradient of Himmelbalu's function ****//
  cout << "\nGradient of Himmelbalu's function:\n" << endl;
  N=0;
  newton_root(&grad_Himmel, x, x0, dx, epsilon, N);

  cout << "The number of function calls was: " << N << endl;
  cout << "The system has a root at:" << endl;
  cout << "x = \n" << x << endl;
  cout << "Where the function values are:" << endl;
  cout << "f(x) = \n" << grad_Himmel(N,x) << endl;
  cout << "\nWe also have:" << endl;
  cout << "norm(fx) = " << norm(grad_Himmel(N,x)) << endl;
  cout << "Which is below the demanded precision." << endl;


  //********** PART B **********//
  cout << "\n\nPART B\n" << endl;

  cout << "\nGradient of Rosenbrock's Valley function (analytical jacobian):\n" << endl;
  N=0;
  newton_root_jac(grad_Rosen, grad_Rosen_jac, x, x0, epsilon, N);

  cout << "The number of function calls was: " << N << endl;
  cout << "The system has a root at:" << endl;
  cout << "x = \n" << x << endl;
  cout << "Where the function values are:" << endl;
  cout << "f(x) = \n" << grad_Rosen(N,x) << endl;
  cout << "\nWe also have:" << endl;
  cout << "norm(fx) = " << norm(grad_Rosen(N,x)) << endl;
  cout << "Which is below the demanded precision." << endl;

  cout << "\nGradient of Himmelblau's function (analytical jacobian):\n" << endl;
  N=0;
  newton_root_jac(grad_Himmel, grad_Himmel_jac, x, x0, epsilon, N);

  cout << "The number of function calls was: " << N << endl;
  cout << "The system has a root at:" << endl;
  cout << "x = \n" << x << endl;
  cout << "Where the function values are:" << endl;
  cout << "f(x) = \n" << grad_Himmel(N,x) << endl;
  cout << "\nWe also have:" << endl;
  cout << "norm(fx) = " << norm(grad_Himmel(N,x)) << endl;
  cout << "Which is below the demanded precision." << endl;

  return 0;
}
