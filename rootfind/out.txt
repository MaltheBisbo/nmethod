PART A

First set of equatons:

Solving the system of equations:
f0(x,y) = Axy - 1 = 0,
f1(x,y) = exp(-x) + exp(-y) - 1 - 1/A = 0
with A = 10000
The number of function calls was: 45
The system has a root at:
x = 
   1.0982e-05
   9.1061e+00

Where the function values are:
f(x) = 
   2.2204e-16
  -1.1018e-17


We also have:
norm(fx) = 2.22318e-16
Which is below the demanded precision.

Gradient of Rosenbrock's Valley function:

The number of function calls was: 2677
The system has a root at:
x = 
   1.0000
   1.0000

Where the function values are:
f(x) = 
   7.4118e-13
  -8.8818e-14


We also have:
norm(fx) = 7.46488e-13
Which is below the demanded precision.

Gradient of Himmelbalu's function:

The number of function calls was: 28
The system has a root at:
x = 
   3.0000
   2.0000

Where the function values are:
f(x) = 
        0
        0


We also have:
norm(fx) = 0
Which is below the demanded precision.


PART B


Gradient of Rosenbrock's Valley function (analytical jacobian):

The number of function calls was: 2043
The system has a root at:
x = 
   1.0000
   1.0000

Where the function values are:
f(x) = 
        0
        0


We also have:
norm(fx) = 0
Which is below the demanded precision.

Gradient of Himmelblau's function (analytical jacobian):

The number of function calls was: 14
The system has a root at:
x = 
   3.0000
   2.0000

Where the function values are:
f(x) = 
        0
        0


We also have:
norm(fx) = 0
Which is below the demanded precision.
