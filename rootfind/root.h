#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_H
#define HEADER_H

using namespace std;
using namespace arma;

vec funcs1(int &N, vec &x);
vec grad_Rosen(int &N, vec &x);
vec grad_Himmel(int &N, vec &x);
mat funcs1_jac(vec &x);
mat grad_Rosen_jac(vec &u);
mat grad_Himmel_jac(vec &u);
void newton_root(vec (*f)(int&,vec&), vec &x, vec &xguess, double dx, double epsilon, int &N);
void newton_root_jac(vec (*f)(int&,vec&), mat (*jacobi)(vec&), vec &x, vec &x0, double epsilon, int &N);

void backsub(mat& R, vec& c, vec& x);
void givens_qr(mat &A);
void givens_qr_QTvec(mat &QR, vec &v);
void givens_qr_solve(mat &A, vec &v, vec &x);

#endif
