#include "root.h"

vec funcs1(int &N, vec &u){
  N++;
  double A = 10000;
  vec f(2);
  double x=u(0), y=u(1);
  f(0) = A*x*y-1;
  f(1) = exp(-x) + exp(-y) - 1 - 1.0/A;
  return f;
}

mat funcs1_jac(vec &u){
  double A = 10000;
  mat jac(2,2);
  double x=u(0), y=u(1);
  jac(0,0) = A*y;
  jac(0,1) = A*x;
  jac(1,0) = -x*exp(-x);
  jac(1,1) = -y*exp(-y);
  return jac;
}

// Gradient of Rosenbrock's valley function.
vec grad_Rosen(int &N, vec &u){
  N++;
  vec dfdu(2);
  double x=u(0), y=u(1);
  dfdu(0) = -2*(1-x) - 400*x*(y-x*x);
  dfdu(1) = 200*(y-x*x);
  return dfdu;
}

mat grad_Rosen_jac(vec &u){
  mat jac(2,2);
  double x=u(0), y=u(1);
  jac(0,0) = 2.0-400.0*y + 1200.0*x*x;
  jac(0,1) = -400.0*x;
  jac(1,0) = -400.0*x;
  jac(1,1) = 200.0;
  return jac;
}

// Gradient of Himmelbalu's function.
vec grad_Himmel(int &N, vec &u){
  N++;
  vec dfdu(2);
  double x=u(0), y=u(1);
  dfdu(0) = 4*x*(x*x+y-11)+2*(x+y*y-7);
  dfdu(1) = 2*(x*x + y - 11) + 4*y*(x + y*y - 7);
  return dfdu;
}


mat grad_Himmel_jac(vec &u){
  mat jac(2,2);
  double x=u(0), y=u(1);
  jac(0,0) = 4.0*(x*x*3.0+y-11.0)+2.0;
  jac(0,1) = 4.0*x+4.0*y;
  jac(1,0) = 4.0*x+4.0*y;
  jac(1,1) = 2.0+4.0*(x+3.0*y*y-7.0);
  return jac;
}




//vec (*f)(vec&)
void newton_root(vec (*f)(int&,vec&), vec &x, vec &x0, double dx, double epsilon, int &N){
  int n = x.n_elem;
  vec x_step(n), fx0(n);
  mat J(n, n);
  vec fx , fx_minus;
  x = x0;

  do {
    // calculate Jacobi matrix numerically
    fx = f(N,x);
    for(int j=0; j<n; j++){
      x(j)+=dx;
      vec df = f(N,x) - fx;
      for(int i=0; i<n; i++){
        J(i,j) = df(i)/dx;
      }
      x(j)-=dx;
    }
    // solve J(dx_vec) = -fx for dx_vec
    fx_minus = -fx;
    givens_qr_solve(J,fx_minus,x_step);
    // determine lambda and make lambda*dx_vec step.
    vec y;
    double lambda = 2.0;
    do{
      lambda /= 2.0;
      y = x+x_step*lambda;
    } while(norm(f(N,y)) > (1-lambda/2.0)*norm(fx) && lambda > 0.02);
    x = y;
  } while(norm(fx) > epsilon);
}



void newton_root_jac(vec (*f)(int&, vec&), mat (*jacobi)(vec&), vec &x, vec &x0, double epsilon, int &N){
  int n = x.n_elem;
  vec x_step(n), fx0(n);
  mat J(n, n);
  vec fx , fx_minus;
  x = x0;

  do {
    // calculate Jacobi matrix numerically
    fx = f(N,x);
    J = jacobi(x);
    // solve J(dx_vec) = -fx for dx_vec
    fx_minus = -fx;
    givens_qr_solve(J,fx_minus,x_step);
    // determine lambda and make lambda*dx_vec step.
    vec y;
    double lambda = 2.0;
    do{
      lambda /= 2.0;
      y = x+x_step*lambda;
    } while(norm(f(N,y)) > (1-lambda/2.0)*norm(fx) && lambda > 0.02);
    x = y;
  } while(norm(fx) > epsilon);
}

void backsub(mat& R, vec& c, vec& x){ // solves Rx = c
  int n = x.size();
  for(int i=n-1; i>=0; i--){
    x(i) = c(i); //Q.col(i) =Qt.row(i)
    for(int j=i+1; j<n; j++){
      x(i) -= R(i,j)*x(j);
    }
    x(i) /= R(i,i);
  }
}

void givens_qr(mat &A){
  int m = A.n_cols;
  int n = A.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = atan(A(p,q)/A(q,q));
      for(int k=q; k<m; k++){
        double rq = A(q,k), rp = A(p,k);
        A(q,k) = cos(theta)*rq + sin(theta)*rp;
        A(p,k) = -sin(theta)*rq + cos(theta)*rp;
      }
      A(p,q) = theta;
    }
  }
}

// Calculates right part of Rx = Q^Tb
void givens_qr_QTvec(mat &QR, vec &v){
  int m = QR.n_cols;
  int n = QR.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = QR(p,q);
      double vq = v(q), vp = v(p);
      v(q) = cos(theta)*vq + sin(theta)*vp;
      v(p) = -sin(theta)*vq + cos(theta)*vp;
    }
  }
}

// solve Rx = Q^Tb by backsubstitution.
void givens_qr_solve(mat &A, vec &v, vec &x){
  givens_qr(A);
  givens_qr_QTvec(A,v);
  backsub(A,v,x);
}
