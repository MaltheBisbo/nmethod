#include "least_square.h"

void qr_gs_decomp(mat &A, mat &R, mat &Q){
  int m = A.n_cols;
  for(int i=0; i<m; i++){
    R(i,i) = norm(A.col(i));
    Q.col(i) = A.col(i)/R(i,i);
    for(int j=i+1; j<m; j++){
      R(i,j) = dot(Q.col(i),A.col(j));
      A.col(j) -= R(i,j)*Q.col(i);
    }
  }
}

void backsub(mat& R, vec& b, vec& c){ // solves Rx = c Rc = b
  int n = c.n_elem;
  for(int i=n-1; i>=0; i--){
    c(i) = b(i); //Q.col(i) =Qt.row(i)
    for(int j=i+1; j<n; j++){
      c(i) -= R(i,j)*c(j);
    }
    c(i) /= R(i,i);
  }
}

double functions(int i, double x){
  switch(i){
    case 0: return 1/x; break;
    case 1: return 1.0; break;
    case 2: return x; break;
    default: {fprintf(stderr,"funs: wrong i=%d",i); return NAN;}
  }
}

void ordinary_least_square_QR(vec &x, vec &y, vec &dy, double (*f)(int, double), vec &c, mat &S){
  int n = x.n_elem;
  int m = 3;
  mat A(n,m);
  vec b(n);
  for(int i=0; i<n; i++){
    b(i) = y(i)/dy(i);
    for(int j=0; j<3; j++){
      A(i,j) = f(j,x(i))/dy(i);
    }
  }
  mat R(3,3,fill::zeros);
  mat Q(n,3,fill::zeros);
  qr_gs_decomp(A,R,Q);
  S = (R.t()*R).i();
  vec d = Q.t()*b;
  backsub(R, d, c);

}

//*** SVD ***//

/*
void SVD(mat &A, mat &U, mat &V){
  int n = A.n_rows;
  int m = A.n_cols;
  if(n>m){
    mat Q(n,m,fill::zeros);
    mat R(m,m,fill::zeros);
    qr_gs_decomp(A,R,Q);
    A=R;
  }
  for(int p=0; p<m; p++){
    for(int q=p+1; q<m; q++){
      double App = A(p,p), Aqq = A(q,q), Apq = A(p,q), Aqp = A(q,p);
      double theta = atan2(Aqp-Apq,App+Aqq);
      double s=sin(theta), c=cos(theta);
    }
  }
}
*/
