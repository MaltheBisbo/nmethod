#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_H
#define HEADER_H

using namespace std;
using namespace arma;

void qr_gs_decomp(mat &A, mat &R, mat &Q);
double functions(int i, double x);
void ordinary_least_square_QR(vec &x, vec &y, vec &dy, double (*f)(int, double), vec &c, mat &S);


#endif
