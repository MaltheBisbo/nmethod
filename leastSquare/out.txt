The coefficient vector is:
c = 
   0.9811
   2.1595
   3.0227

The covariance matrix is:
S = 
   0.0093  -0.0521   0.0295
  -0.0521   0.3496  -0.2095
   0.0295  -0.2095   0.1601

