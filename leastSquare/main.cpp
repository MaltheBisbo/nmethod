#include "least_square.h"
int main(){
  //********** PART A+B **********//

  //*** Define data ***//

  vec x({0.100, 0.145, 0.211, 0.307, 0.447, 0.649, 0.944, 1.372, 1.995, 2.900});
  vec y({12.644, 9.235, 7.377, 6.460, 5.555, 5.896, 5.673, 6.964, 8.896, 11.355});
  vec dy({0.858, 0.359, 0.505, 0.403, 0.683, 0.605, 0.856, 0.351, 1.083, 1.002});
  int N=x.n_elem;
  int Nfuncs = 3;
  vec c(Nfuncs);
  mat S(Nfuncs,Nfuncs);
  //*** Open datafile ***//
  ofstream file;
  file.open("data.dat",ios::trunc);

  //*** Make ordinary least squares fit + check results ***//
  ordinary_least_square_QR(x, y, dy, &functions, c, S);
  cout << "The coefficient vector is:" << endl;
  cout << "c = \n" << c << endl;
  cout << "The covariance matrix is:" << endl;
  cout << "S = \n" << S << endl;

  //*** Plot data ***//
  file << "x_data\ty_data\tdy_data" << endl;
  for(int i=0; i<N; i++){
    file << x(i) << "\t" << y(i) << "\t" << dy(i) << endl;
  }

  //*** Plot fit + variations ***//
  file << "\n\nx\tf\tf_minus\tf_plus" << endl;
  int Npoints = 100;
  double xmax = 3;
  Col<double> fct(Nfuncs);
  Row<double> fctT(Nfuncs);
  for(int i=3; i<Npoints; i++){
    double x = i*xmax/Npoints;
    double f = 0;
    for(int j=0; j<3; j++){
      fct(j) = functions(j,x);
      fctT(j) = fct(j);
      f+=c(j)*fct(j);
    }
    double df = sqrt(as_scalar(fctT*S*fct));
    file << x << "\t" << f << "\t" << f-df << "\t" << f+df << endl;
  }

  //********** PART C **********//

  return 0;
}
