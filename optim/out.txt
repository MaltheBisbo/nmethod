§§§--- PART A ---§§§

-----Rosenbrock

The function has a root at;
x = 
   1.0000
   1.0000

with f(x) = 3.46911e-22
Number of iterations: 33

-----Himmelblau

The function has a root at;
x = 
  -3.7793
  -3.2832

with f(x) = 7.88861e-31
Number of iterations: 5

§§§--- PART B ---§§§

-----Rosenbrock (numerical Hessian)

The function has a root at;
x = 
   1.0000
   1.0000

with f(x) = 7.81278e-21
Number of iterations: 44646

-----Himmelblau (numerical Hessian)

The function has a root at;
x = 
  -3.7793
  -3.2832

with f(x) = 1.76153e-20
Number of iterations: 11

-----Rosenbrock (numerical Hessian + gradient)

The function has a root at;
x = 
   1.0000
   1.0000

with f(x) = 9.03247e-12
Number of iterations: 103320

-----Himmelblau (numerical Hessian + gradient)

The function has a root at;
x = 
  -3.7793
  -3.2832

with f(x) = 3.52911e-15
Number of iterations: 11

-----Fit (numerical Hessian + gradient)

The function has a root at;
params = 
   3.5570
   3.2054
   1.2319

Number of iterations: 255211

§§§--- PART C ---§§§

-----Rosenbrock (Simplex)-----

Initial simplex is:
S0 =
   5.0000   7.5000  -2.0000
   2.0000   6.5000   9.5000

The function has a root at;
x = 
   1.0000
   1.0000

with f(x) = 3.66502e-20
Number of iterations: 159

-----Himmelblau (Simplex)-----

Initial simplex is:
S0 =
   5.0000   7.5000  -2.0000
   2.0000   6.5000   9.5000

The function has a root at;
x = 
  -3.7793
  -3.2832

with f(x) = 1.10349e-18
Number of iterations: 86

-----Fit (Simplex)-----

Initial simplex is:
S0 =
   5.0000   7.5000  -2.0000   9.0000
   2.0000   6.5000   9.5000   1.0000
   1.0000  -2.0000   3.0000   4.0000

The data is best fitted with parameters:
params = 
   3.5570
   3.2054
   1.2319

Number of iterations: 159
