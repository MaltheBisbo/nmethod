#include "optim.h"

double Rosen(vec &u){
  double f;
  double x=u(0), y=u(1);
  f = pow((1-x),2)+100*pow(y-x*x,2);
  return f;
}

vec Rosen_grad(vec &u){
  vec dfdu(2);
  double x=u(0), y=u(1);
  dfdu(0) = -2*(1-x) - 400*x*(y-x*x);
  dfdu(1) = 200*(y-x*x);
  return dfdu;
}

mat Rosen_Hess(vec &u){
  mat jac(2,2);
  double x=u(0), y=u(1);
  jac(0,0) = 2.0-400.0*y + 1200.0*x*x;
  jac(0,1) = -400.0*x;
  jac(1,0) = -400.0*x;
  jac(1,1) = 200.0;
  return jac;
}

double Himmel(vec &u){
  double f;
  double x=u(0), y=u(1);
  f = pow(x*x+y-11,2)+pow(x+y*y-7,2);
  return f;
}

vec Himmel_grad(vec &u){
  vec dfdu(2);
  double x=u(0), y=u(1);
  dfdu(0) = 4*x*(x*x+y-11)+2*(x+y*y-7);
  dfdu(1) = 2*(x*x + y - 11) + 4*y*(x + y*y - 7);
  return dfdu;
}


mat Himmel_Hess(vec &u){
  mat jac(2,2);
  double x=u(0), y=u(1);
  jac(0,0) = 4.0*(x*x*3.0+y-11.0)+2.0;
  jac(0,1) = 4.0*x+4.0*y;
  jac(1,0) = 4.0*x+4.0*y;
  jac(1,1) = 2.0+4.0*(x+3.0*y*y-7.0);
  return jac;
}

double fit_func(double t, vec& params){
  //vec par(3) = *(vec*)params;
  double A = params(0), T = params(1), B = params(2);
  double f = A*exp(-t/T)+B;
  return f;
}

double fit(vec& params){
  vec t = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
  vec y = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
  vec e = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
  double F = 0;
  int Nt = t.n_elem;
  for(int i=0; i<Nt; i++){
    F += pow(fit_func(t(i),params)-y(i) ,2)/(e(i)*e(i));
  }
  return F;
}

vec Newton_optim(double (*f)(vec&), vec (*grad)(vec&), mat (*Hess)(vec&), vec &x0, double epsilon, int &N){
  int n = x0.n_elem;
  vec x_step(n), gradx;
  vec x = x0;
  mat H;
  double fx;
  double alpha = 1e-4;

  do {
    N++;
    fx = f(x);
    gradx = grad(x);
    H = Hess(x);
    // solve H(dx_vec) = -grad_f for dx_vec
    vec grad_minus = -grad(x);
    givens_qr_solve(H,grad_minus,x_step);
    // determine lambda and make lambda*dx_vec step.
    vec y;
    double lambda = 2.0;
    do{
      lambda /= 2.0;
      y = x+x_step*lambda;
    } while(f(y) > (fx + alpha*lambda*as_scalar(x_step.t()*gradx)) && lambda > 0.02);
    x = y;
  } while(norm(grad(x)) > epsilon);
  return x;
}


vec Newton_optim1(double (*f)(vec&), vec (*grad)(vec&), vec &x0, double epsilon, int &N){
  int n = x0.n_elem;
  vec x_step(n), gradx;
  vec x = x0, s, z, y;
  mat H1=eye(n,n);
  double fx;
  double alpha = 1e-2, dx = 1e-6;

  do {
    N++;
    fx = f(x);
    gradx = grad(x);
    x_step = -H1*gradx;
    // determine lambda and make lambda*dx_vec step.
    //cerr << "x = \n" << x << endl;
    //cerr << "x_step = \n" << x_step << endl;
    s = x_step*2;
    do{
      s /=2.0;
      z = x+s;
      if(norm(s) < dx){
        H1.eye();
        break;
      }
    } while(abs(f(z)) > (abs(fx) + alpha*as_scalar(s.t()*gradx)));
    x = z;
    y = grad(x)-gradx;
    H1 = H1 + (s-H1*y)*s.t()*H1/as_scalar(y.t()*H1*s);
  } while(norm(grad(x)) > epsilon);
  return x;
}


vec gradient(double (*f)(vec&), vec& x, double dx){
  int n = x.n_elem;
  vec dfdx(n);
  double fx = f(x);
  for(int i=0; i<n; i++){
    x(i) += dx;
    dfdx(i) = (f(x)-fx)/dx;
    x(i) -= dx;
  }
  return dfdx;
}


vec Newton_optim2(double (*f)(vec&), vec &x0, double epsilon, int &N){
  int n = x0.n_elem;
  vec x_step(n), gradx;
  vec x = x0, s, z, y;
  mat H1=eye(n,n);
  double fx;
  double alpha = 1e-2, dx = 1e-8, ds = 1e-6;

  do {
    N++;
    fx = f(x);
    gradx = gradient(f,x,dx);
    x_step = -H1*gradx;
    // determine lambda and make lambda*dx_vec step.
    //cerr << "x = \n" << x << endl;
    //cerr << "x_step = \n" << x_step << endl;
    s = x_step*2;
    do{
      s /=2.0;
      z = x+s;
      if(norm(s) < ds){
        H1.eye();
        break;
      }
    } while(abs(f(z)) > (abs(fx) + alpha*as_scalar(s.t()*gradx)));
    x = z;
    y = gradient(f,x,dx)-gradx;
    H1 = H1 + (s-H1*y)*s.t()*H1/as_scalar(y.t()*H1*s);
  } while(norm(gradient(f,x,dx)) > epsilon);
  return x;
}


void update_simplex(mat &simplex, vec &fval, int d, int &hi, int &lo, vec &centroid){
  hi = max_index(fval); lo = min_index(fval);
  centroid.zeros();
  for(int i=0; i<d+1; i++){
    if(i != hi){ centroid += simplex.col(i); }
  }
  centroid /= d;
}

void initiate_simplex(double f(vec&), mat &simplex, vec &fval, int d, int &hi, int &lo, vec &centroid){
  for(int i=0; i<d+1; i++){ vec simplex_i = simplex.col(i); fval(i) = f(simplex_i); }
  update_simplex(simplex, fval, d, hi, lo, centroid);
}

void reflection(vec simplex_highest, vec centroid ,vec &reflected){
  reflected = 2*centroid - simplex_highest;
}

void expansion(vec simplex_highest, vec centroid ,vec &expanded){
  expanded = 3*centroid - 2*simplex_highest;
}

void contraction(vec simplex_highest, vec centroid ,vec &contracted){
  contracted = (simplex_highest + centroid)/2;
}

void reduction(mat &simplex , int d, int index_lo){
  for(int i=0; i<d+1; i++){
    if(i!=index_lo){
      simplex.col(i) = (simplex.col(i)+simplex.col(index_lo))/2;
    }
  }
}

double simplex_size(mat simplex, int d){
  double size = 0, dist;
  for(int i=1; i<d+1; i++){
    vec diff = simplex.col(0)-simplex.col(i);
    dist = sqrt(dot(diff,diff));
    if(dist > size){size=dist;}
  }
  return size;
}


int downhill_simplex(double f(vec&), mat &simplex, int d, double size_goal){
  int hi,lo, k=0;
  vec p1, p2;
  vec centroid(d), fval(d+1), highest(d);
  initiate_simplex(f, simplex, fval, d, hi, lo, centroid);
  double f_re, f_ex, f_co;

  while(simplex_size(simplex, d) > size_goal){
    update_simplex(simplex, fval, d, hi, lo, centroid);
    highest = simplex.col(hi);
    reflection(highest, centroid, p1); // try reflection
    f_re = f(p1);
    if( f_re<fval(lo) ){
      expansion(highest, centroid, p2); // try expansion (reflection followed bu expansion)
      f_ex = f(p2);
      if(f_ex<f_re){  // accept expansion
        simplex.col(hi) = p2; fval(hi) = f_ex;
      } else{ // accept reflection
        simplex.col(hi) = p1; fval(hi) = f_re;
      }
    } else{
      if( f_re<fval(hi) ){ // accept reflection
        simplex.col(hi) = p1; fval(hi) = f_re;
      } else{
        contraction(highest, centroid, p2); // try contraction
        f_co = f(p2);
        if( f_co<fval(hi) ){ // accept contraction
          simplex.col(hi) = p2; fval(hi) = f_co;
        } else{ // do reduction
          reduction(simplex, d, lo);
          initiate_simplex(f, simplex, fval, d, hi, lo, centroid);
        }
      }
    }
    k++;
  }
  return k;
}

int min_index(vec &x){
  int n=x.n_elem, i_min=0;
  double minval = x(0);
  for(int i=1; i<n; i++){
    if(x(i)<minval){ minval=x(i); i_min=i; }
  }
  return i_min;
}

int max_index(vec &x){
  int n=x.n_elem, i_max=0;
  double maxval = x(0);
  for(int i=1; i<n; i++){
    if(x(i)>maxval){ maxval=x(i); i_max=i; }
  }
  return i_max;
}

void backsub(mat& R, vec& c, vec& x){ // solves Rx = c
  int n = x.size();
  for(int i=n-1; i>=0; i--){
    x(i) = c(i); //Q.col(i) =Qt.row(i)
    for(int j=i+1; j<n; j++){
      x(i) -= R(i,j)*x(j);
    }
    x(i) /= R(i,i);
  }
}

void givens_qr(mat &A){
  int m = A.n_cols;
  int n = A.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = atan(A(p,q)/A(q,q));
      for(int k=q; k<m; k++){
        double rq = A(q,k), rp = A(p,k);
        A(q,k) = cos(theta)*rq + sin(theta)*rp;
        A(p,k) = -sin(theta)*rq + cos(theta)*rp;
      }
      A(p,q) = theta;
    }
  }
}

// Calculates right part of Rx = Q^Tb
void givens_qr_QTvec(mat &QR, vec &v){
  int m = QR.n_cols;
  int n = QR.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = QR(p,q);
      double vq = v(q), vp = v(p);
      v(q) = cos(theta)*vq + sin(theta)*vp;
      v(p) = -sin(theta)*vq + cos(theta)*vp;
    }
  }
}

// solve Rx = Q^Tb by backsubstitution.
void givens_qr_solve(mat &A, vec &v, vec &x){
  givens_qr(A);
  givens_qr_QTvec(A,v);
  backsub(A,v,x);
}
