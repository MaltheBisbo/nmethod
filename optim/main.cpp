#include "optim.h"


int main(){

  vec x, x0 = {-4,-4};
  double epsilon = 1e-8;
  int N = 0;

  cout << "§§§--- PART A ---§§§" << endl;

  cout << "\n-----Rosenbrock\n" << endl;
  x = Newton_optim(&Rosen,&Rosen_grad,&Rosen_Hess,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << x << endl;
  cout << "with f(x) = " << Rosen(x) << endl;
  cout << "Number of iterations: " << N << endl;

  N = 0;
  cout << "\n-----Himmelblau\n" << endl;
  x = Newton_optim(&Himmel,&Himmel_grad,&Himmel_Hess,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << x << endl;
  cout << "with f(x) = " << Himmel(x) << endl;
  cout << "Number of iterations: " << N << endl;

  cout << "\n§§§--- PART B ---§§§" << endl;

  N = 0;
  cout << "\n-----Rosenbrock (numerical Hessian)\n" << endl;
  x = Newton_optim1(&Rosen,&Rosen_grad,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << x << endl;
  cout << "with f(x) = " << Rosen(x) << endl;
  cout << "Number of iterations: " << N << endl;

  N = 0;
  cout << "\n-----Himmelblau (numerical Hessian)\n" << endl;
  x = Newton_optim1(&Himmel,&Himmel_grad,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << x << endl;
  cout << "with f(x) = " << Himmel(x) << endl;
  cout << "Number of iterations: " << N << endl;

  N = 0;
  cout << "\n-----Rosenbrock (numerical Hessian + gradient)\n" << endl;
  x = Newton_optim2(&Rosen,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << x << endl;
  cout << "with f(x) = " << Rosen(x) << endl;
  cout << "Number of iterations: " << N << endl;

  N = 0;
  cout << "\n-----Himmelblau (numerical Hessian + gradient)\n" << endl;
  x = Newton_optim2(&Himmel,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << x << endl;
  cout << "with f(x) = " << Himmel(x) << endl;
  cout << "Number of iterations: " << N << endl;

  N = 0;
  x0 = {1,1,1};
  cout << "\n-----Fit (numerical Hessian + gradient)\n" << endl;
  vec params = Newton_optim2(&fit,x0,epsilon,N);
  cout << "The function has a root at;" << endl;
  cout << "params = \n" << params << endl;
  cout << "Number of iterations: " << N << endl;

  ofstream file;
  file.open("data.dat",ios::trunc);

  vec t = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
  vec y = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
  vec e = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};

  file << "t\ty\te"  << endl;
  int Nt = t.n_elem;
  for(int i=0; i<Nt; i++){
    file << t(i) << "\t" << y(i) << "\t" << e << endl;
  }

  file << "\n\nt_fit" << "\t" << "y_fit" << endl;
  for(int i=0; i<101; i++){
    double t_fit = i*0.1;
    double y_fit = fit_func(t_fit, params);
    file << t_fit << "\t" << y_fit << endl;
  }

  cout << "\n§§§--- PART C ---§§§" << endl;

  cout << "\n-----Rosenbrock (Simplex)-----\n" << endl;
  int d=2;
  mat simplex(d,d+1);
  simplex(0,0) = 5;
  simplex(1,0) = 2;
  simplex(0,1) = 7.5;
  simplex(1,1) = 6.5;
  simplex(0,2) = -2;
  simplex(1,2) = 9.5;
  cout << "Initial simplex is:" << endl;
  cout << "S0 =\n" << simplex << endl;

  int Niter = downhill_simplex(Rosen, simplex, d, 1e-9);

  vec simplex0 = simplex.col(0);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << simplex0 << endl;
  cout << "with f(x) = " << Rosen(simplex0) << endl;
  cout << "Number of iterations: " << Niter << endl;

  cout << "\n-----Himmelblau (Simplex)-----\n" << endl;
  d=2;
  simplex(0,0) = 5;
  simplex(1,0) = 2;
  simplex(0,1) = 7.5;
  simplex(1,1) = 6.5;
  simplex(0,2) = -2;
  simplex(1,2) = 9.5;
  cout << "Initial simplex is:" << endl;
  cout << "S0 =\n" << simplex << endl;

  Niter = downhill_simplex(Himmel, simplex, d, 1e-9);

  simplex0 = simplex.col(0);
  cout << "The function has a root at;" << endl;
  cout << "x = \n" << simplex0 << endl;
  cout << "with f(x) = " << Himmel(simplex0) << endl;
  cout << "Number of iterations: " << Niter << endl;

  cout << "\n-----Fit (Simplex)-----\n" << endl;
  d=3;
  mat simplex_fit(d,d+1);
  simplex_fit(0,0) = 5;
  simplex_fit(1,0) = 2;
  simplex_fit(2,0) = 1;
  simplex_fit(0,1) = 7.5;
  simplex_fit(1,1) = 6.5;
  simplex_fit(2,1) = -2;
  simplex_fit(0,2) = -2;
  simplex_fit(1,2) = 9.5;
  simplex_fit(2,2) = 3;
  simplex_fit(0,3) = 9;
  simplex_fit(1,3) = 1;
  simplex_fit(2,3) = 4;
  cout << "Initial simplex is:" << endl;
  cout << "S0 =\n" << simplex_fit << endl;

  Niter = downhill_simplex(fit, simplex_fit, d, 1e-9);

  vec simplex0_fit = simplex_fit.col(0);
  cout << "The data is best fitted with parameters:" << endl;
  cout << "params = \n" << simplex0_fit << endl;
  cout << "Number of iterations: " << Niter << endl;


  return 0;
}
