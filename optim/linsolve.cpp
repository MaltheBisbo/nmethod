#include "optim.h"

void backsub(mat& R, vec& c, vec& x){ // solves Rx = c
  int n = x.size();
  for(int i=n-1; i>=0; i--){
    x(i) = c(i); //Q.col(i) =Qt.row(i)
    for(int j=i+1; j<n; j++){
      x(i) -= R(i,j)*x(j);
    }
    x(i) /= R(i,i);
  }
}

void givens_qr(mat &A){
  int m = A.n_cols;
  int n = A.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = atan(A(p,q)/A(q,q));
      for(int k=q; k<m; k++){
        double rq = A(q,k), rp = A(p,k);
        A(q,k) = cos(theta)*rq + sin(theta)*rp;
        A(p,k) = -sin(theta)*rq + cos(theta)*rp;
      }
      A(p,q) = theta;
    }
  }
}

// Calculates right part of Rx = Q^Tb
void givens_qr_QTvec(mat &QR, vec &v){
  int m = QR.n_cols;
  int n = QR.n_rows;

  for(int q=0; q<m; q++){
    for(int p=q+1; p<n; p++){
      double theta = QR(p,q);
      double vq = v(q), vp = v(p);
      v(q) = cos(theta)*vq + sin(theta)*vp;
      v(p) = -sin(theta)*vq + cos(theta)*vp;
    }
  }
}

// solve Rx = Q^Tb by backsubstitution.
void givens_qr_solve(mat &A, vec &v, vec &x){
  givens_qr(A);
  givens_qr_QTvec(A,v);
  backsub(A,v,x);
}
