#include <armadillo>
#include <iostream>
#include <math.h>

#ifndef HEADER_H
#define HEADER_H

using namespace std;
using namespace arma;

double Rosen(vec &u);
vec Rosen_grad(vec &u);
mat Rosen_Hess(vec &u);

double Himmel(vec &u);
vec Himmel_grad(vec &u);
mat Himmel_Hess(vec &u);

double fit_func(double t, vec& params);
double fit(vec& params);

vec gradient(double (*f)(vec&), vec& x, double dx);
vec Newton_optim(double (*f)(vec&), vec (*grad)(vec&), mat (*Hess)(vec&), vec &x0, double epsilon, int &N);
vec Newton_optim1(double (*f)(vec&), vec (*grad)(vec&), vec &x0, double epsilon, int &N);
vec Newton_optim2(double (*f)(vec&), vec &x0, double epsilon, int &N);

void reflection(vec simplex_highest, vec centroid ,vec &reflected);
void expansion(vec simplex_highest, vec centroid ,vec &expanded);
void contraction(vec simplex_highest, vec centroid ,vec &contracted);
void reduction(mat &simplex , int d, int index_lo);
double simplex_size(mat simplex, int d);
void update_simplex(mat &simplex, vec &fval, int d, int &hi, int &lo, vec &centroid);
void initiate_simplex(double f(vec&), mat &simplex, vec &fval, int d, int &hi, int &lo, vec &centroid);
int downhill_simplex(double f(vec&), mat &simplex, int d, double size_goal);

int min_index(vec &x);
int max_index(vec &x);

void backsub(mat& R, vec& c, vec& x);
void givens_qr(mat &A);
void givens_qr_QTvec(mat &QR, vec &v);
void givens_qr_solve(mat &A, vec &v, vec &x);

#endif
